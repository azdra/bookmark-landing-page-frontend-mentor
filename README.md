# Challenge Frontend Mentor
<a href=https://www.frontendmentor.io/solutions/bookmark-landing-page-with-htmlscss-and-js-tkvrNhxWm>The challenge: Bookmark landing page</a>

Technologies used:
<ul>
    <li>HTML</li>
    <li>Bootstrap JS</li>
    <li>Bootstrap CSS</li>
    <li>SCSS</li>
</ul>

Preview
![img.png](img.png)
![img_2.png](img_2.png)
![img_3.png](img_3.png)
